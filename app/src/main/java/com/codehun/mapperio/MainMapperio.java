package com.codehun.mapperio;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.codehun.mapperio.com.codehun.general.AppMarkerManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainMapperio extends FragmentActivity implements OnMapReadyCallback, OnMarkerClickListener, GoogleMap.OnMapLongClickListener {

    private final LatLng FRANCE = new LatLng(48.858093,2.294694);
    private final LatLng AMAZONAS = new LatLng(-2.478228, -62.672689);

    private AppMarkerManager markerManager;
    private MapFragment map;
    private GoogleMap gMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mapperio);

        markerManager = new AppMarkerManager();

        map = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        map.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMapLongClickListener(this);
        gMap = googleMap;

        Marker eiffel = googleMap.addMarker(new MarkerOptions().position(FRANCE));
        markerManager.addAppMarker(eiffel, "com.codehun.eiffelAR");


        Marker brazil = googleMap.addMarker(new MarkerOptions().position(AMAZONAS));
        markerManager.addAppMarker(brazil, "com.codehun.run");
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        String appPackage = markerManager.getAppMarkerPackage(marker);
        Intent intent = getPackageManager().getLaunchIntentForPackage(appPackage);
        boolean startedActivity = false;
        if(intent != null){
            startActivity(intent);
            startedActivity = true;
        }
        return startedActivity;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }
}
