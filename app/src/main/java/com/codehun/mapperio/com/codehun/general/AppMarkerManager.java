package com.codehun.mapperio.com.codehun.general;

import android.util.ArrayMap;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by Juanto on 9/18/2015.
 */
public class AppMarkerManager {

    ArrayMap<Marker,String> markers;

    public AppMarkerManager(){
        this.markers = new ArrayMap<>();
    }

    public void addAppMarker(Marker marker, String appPackage){
        this.markers.put(marker,appPackage);
    }

    public String getAppMarkerPackage(Marker marker){
        return this.markers.get(marker);
    }
}
